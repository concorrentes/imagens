#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>

#define __GROUPSIZE__		5		//dimensao do quadrado para a media
#define __STEP__		(__GROUPSIZE__/2) //dimensao do pulo

//precisa escrever a imagem out?
#define WRITE_IMG_OUT	0

using namespace cv;

/**
 * Funcao que pega a media dos pixels ao redor
 * @param  img imagem original
 * @param  x   posicao x
 * @param  y   posicao y
 */
void getAverage(Mat* img, int x, int y, unsigned char* result){

	int counter=0;	 //quantidade de pixels para a media
	int b=0,g=0,r=0; //acumuladores

	for(int i = x - __STEP__; i <= x + __STEP__; i++){
		Vec3b* pixel_array = img->ptr<Vec3b>(i);
		for(int j = y - __STEP__; j <= y + __STEP__; j++){
			if(i < 0 || i >= img->rows || j < 0 || j >= img->cols ){
			//se estamos fora da imagem (miss)
			} else {
				b += pixel_array[j][0];
				g += pixel_array[j][1];
				r += pixel_array[j][2];
				counter++;
			}
		}
	}
	
	result[0] = b / counter;
	result[1] = g / counter;
	result[2] = r / counter;
}

/**
 * Funcao que faz um smooth da imagem original
 * @param  origin imagem original
 * @return        nova imagem processada
 */
Mat* smooth(Mat *origin){

	Mat *modified = new Mat(origin->rows, origin->cols, origin->type());
	unsigned char *buffer = (unsigned char*) calloc(3, sizeof(unsigned char));
	
	for (int i = 0; i < origin->rows; i++){
		
		//ponteiro para dados
		Vec3b* pixel_array = modified->ptr<Vec3b>(i);

		for (int j = 0; j < origin->cols; j++){

			//realizando a media
			getAverage(origin, i, j, buffer);
			
			//definindo o novo pixel baseado na media
			pixel_array[j][2] = buffer[2];
			pixel_array[j][1] = buffer[1];
			pixel_array[j][0] = buffer[0];
			
		}
	}

	free(buffer);
	return modified;
}

/**
 * Funcao de copia de strin
 * @param  string string a ser copiada
 * @return        copia
 */
char *copyString(char *string){

	char *buffer = (char*) malloc(sizeof(char)*(strlen(string)+1));
	strcpy(buffer, string);

	return buffer;
}

void appendTimeToFile(char *filename, double time){

	FILE* file = fopen(filename, "a+");

	fprintf(file, "%lf\n", time);

	fclose(file);
}

int main(int argc, char **argv){
	
	Mat image; //imagem original
	Mat *image_out; //imagem de saida
	char *name_out;
	clock_t start, end;
	double time;

	if(argc != 3){ //se argumentos nao sao suficientes
		printf("Usage: %s <FILE_IN> <FILE_OUT>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	image = imread(argv[1]); //lendo imagem

	start = clock();

	image_out = smooth(&image); //imagem recebe smooth

	end = clock();

	name_out = copyString(argv[2]);

	char time_filename[50];
	strcpy(time_filename, name_out);
	strcat(time_filename, "_time_main_st.txt");

	time = (double) (end - start)/ CLOCKS_PER_SEC;
	
	appendTimeToFile(time_filename, time);

	if(WRITE_IMG_OUT)
		imwrite(argv[2], *image_out); //imagem de saida e escrita

	delete image_out; //deletando
	free(name_out);

	return 0;
}


