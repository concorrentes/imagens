# Imagens com OPENMP e OPENMPI

## Sinopse
	Aplicação capaz de realizar a paralelização da técnica smoothing em imagens de diferentes 
	tamanhos utilizando técnicas de programação linear simples e de programação paralela.
	Para isso, considerou-se o uso de duas bibliotecas principais: OpenMP e OpenMPI. 
	A primeira, para a programação paralela de um mesmo processador com seus diversos núcleos. 
	Já a segunda, é uma biblioteca para comunicação de processos, seja ele em uma ou em diversas máquinas.
	
## Autores:
	Oscar Lima Neto		oscarneto@usp.br
	Paulo G. De Mitri 	paulo.mitri@usp.br


## Compilar e executar

	AVISO: Testado com opencv 2.4.12.2-1 e openmpi 1.10.0-1
	Entre no subdiretório `src/` e logo após no subdiretório da versão e realize o comando `make` 
	Para rodar o programa com todas as imagens execute o script `./run.sh`.

	O script aceita 2 argumentos:
	`-bin <NOME DO BINÁRIO>` : Esse argumento especifica qual programa deve rodar.
	`-fname <NOME DA IMAGEM>`: Esse argumento especifica qual imagem deve rodar.

	Caso esses argumentos não estejam presentes todas as imagens ou programas serão executados.

	É necessário ainda especificar o numero de processos editando o script.

## Entrada e saída

	O programa retira todas as imagens de um único diretório: `in/`
	Toda a saída é colocada em `out/`